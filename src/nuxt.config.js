import colors from 'vuetify/es5/util/colors'
import koha_api from './koha-api.json'

export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: 'Koha › Plugins › Pay via PayPal',
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@koumoul/vuetify-jsonschema-form/dist/main.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/draggable.js',
    '~/plugins/color.js',
    '~/plugins/swatches.js',
    '~/plugins/i18n.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    ['@nuxtjs/eslint-module', { failOnError: false }],
    '@nuxtjs/vuetify'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseUrl: '/'
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: colors.blueGrey.base,
          secondary: colors.orange.base,
          accent: colors.indigo.base,
          error: colors.pink.base,
          warning: colors.lime.base,
          info: colors.cyan.base,
          success: colors.green.base
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    },
    hotMiddleware: {
      client: {
        overlay: false
      }
    },
    publicPath: koha_api.path+'/static/_nuxt'
  },
  router: {
    extendRoutes (routes, resolve) {
      const allIndex = routes.findIndex(route => route.name === 'all')
      routes[allIndex] = {
        ...routes[allIndex],
        components: {
          default: routes[allIndex].component,
          configure: resolve(__dirname, 'components/configure.vue'),
          logo: resolve(__dirname, 'components/VuetifyLogo.vue')
        },
        chunkNames: {
          configure: 'components/configure',
          logo: 'components/VuetifyLogo'
        }
      }
    }
  }
}
