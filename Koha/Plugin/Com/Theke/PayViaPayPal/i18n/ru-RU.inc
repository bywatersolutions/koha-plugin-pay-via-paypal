/*
	[%
		TOKENS = {
			home_breadcrumb = "Начало"
			your_payment_breadcrumb = "Your Payment"
			error = "Ошибка: "
			error_title = "возникла проблема с обработкой вашего платежа"
			PAYPAL_UNABLE_TO_CONNECT = "Не удаётся добавить одну или несколько меток."
			PAYPAL_TRY_LATER = "Попробуйте ещё раз позднее."
			PAYPAL_ERROR_PROCESSING = "Не удаётся добавить одну или несколько меток."
			PAYPAL_CONTACT_LIBRARY = "Пожалуйста, обратитесь в библиотеку для подтверждения вашего платежа."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Вернуть этот экземпляр "
		}
	%]
*/