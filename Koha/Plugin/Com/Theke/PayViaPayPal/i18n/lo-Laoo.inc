/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "ຜິດພາດ:"
			error_title = "ມີບັນຫາກ່ຽວກັບການສົ່ງຂໍ້ມູນຂອງທ່ານ"
			PAYPAL_UNABLE_TO_CONNECT = "ບໍ່ສາມາດທີ່ຈະເພີ່ມອີກ 1 ຫລື ຫລາຍກວ່າ 1 ແທັກ."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "ບໍ່ສາມາດທີ່ຈະເພີ່ມອີກ 1 ຫລື ຫລາຍກວ່າ 1 ແທັກ."
			PAYPAL_CONTACT_LIBRARY = "ບັດຂອງທ່ານຈະໜົດກຳໃນໃນ {0}. ກະລຸນາຕິດຕໍຫາຫໍສະໜຸດ ຖ້າທ່ານຕ້ອງການທີ່ຈະຕໍ່ອາຍຸບັດຂອງທ່ານ."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "ກັບຄືນຫາລາຍການນີ້ "
		}
	%]
*/