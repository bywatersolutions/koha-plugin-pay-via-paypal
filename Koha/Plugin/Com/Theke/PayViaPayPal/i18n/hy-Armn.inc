/*
	[%
		TOKENS = {
			home_breadcrumb = "Տուն"
			your_payment_breadcrumb = "Your Payment"
			error = "Սխալ"
			error_title = "քո վճարումը իրականացնելիս հանդիպել է խնդիր"
			PAYPAL_UNABLE_TO_CONNECT = "Չի կարող միանալ PayPal-ին։"
			PAYPAL_TRY_LATER = "Խնդրում ենք, ավելի ուշ կրկին փորձել։"
			PAYPAL_ERROR_PROCESSING = "Չի կարող վավերացնել քո վճարումը։"
			PAYPAL_CONTACT_LIBRARY = "Խնդրում եմ կապվիր գրադարանաի հետ, քո վճարումը կվավերացնելու համար։"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Վերադարձիր տուգանքի մանրամասներին"
		}
	%]
*/