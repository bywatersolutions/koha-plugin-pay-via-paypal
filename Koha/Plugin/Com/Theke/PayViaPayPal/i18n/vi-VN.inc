/*
	[%
		TOKENS = {
			home_breadcrumb = "Trang chủ"
			your_payment_breadcrumb = "Your Payment"
			error = "Lỗi:"
			error_title = "Phát hiện lỗi với yêu cầu đổi mật khẩu của bạn"
			PAYPAL_UNABLE_TO_CONNECT = "Không thể thêm từ khóa nữa."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Không thể thêm từ khóa nữa."
			PAYPAL_CONTACT_LIBRARY = ". Xin vui lòng liên hệ với thư viện để biết thêm thông tin."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Quay lại "
		}
	%]
*/