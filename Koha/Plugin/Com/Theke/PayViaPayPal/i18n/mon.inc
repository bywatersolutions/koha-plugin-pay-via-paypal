/*
	[%
		TOKENS = {
			home_breadcrumb = "Үндсэн"
			your_payment_breadcrumb = "Your Payment"
			error = "Алдаа:"
			error_title = "Таны оруулгад асуудал гарлаа"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "Холбоо барих мэдээлэл"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Энэ зүйлийг буцаах "
		}
	%]
*/