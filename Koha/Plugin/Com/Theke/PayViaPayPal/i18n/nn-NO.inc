/*
	[%
		TOKENS = {
			home_breadcrumb = "Heim"
			your_payment_breadcrumb = "Your Payment"
			error = "Feil:"
			error_title = "Det oppsto eit problem med sendinga di"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "Kontaktinformasjon"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Lever dette lånet tilbake "
		}
	%]
*/