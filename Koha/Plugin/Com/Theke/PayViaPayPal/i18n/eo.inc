/*
	[%
		TOKENS = {
			home_breadcrumb = "Hejmo"
			your_payment_breadcrumb = "Your Payment"
			error = "Eraro"
			error_title = "estis problemo dum prilaborado de via pago"
			PAYPAL_UNABLE_TO_CONNECT = "Ne eblas konektiĝi al PayPal."
			PAYPAL_TRY_LATER = "Bonvolu provi denove pli malfrue."
			PAYPAL_ERROR_PROCESSING = "Ne eblas konfirmi la pagon."
			PAYPAL_CONTACT_LIBRARY = "Bonvolu kontakti la bibliotekon por konfirmi vian pagon."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Revenu al detaloj pri monpuno"
		}
	%]
*/