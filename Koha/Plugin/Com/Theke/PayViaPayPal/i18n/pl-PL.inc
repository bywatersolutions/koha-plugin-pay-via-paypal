/*
	[%
		TOKENS = {
			home_breadcrumb = "Strona główna"
			your_payment_breadcrumb = "Your Payment"
			error = "Błąd"
			error_title = "wystąpił problem z przetwarzaniem płatności"
			PAYPAL_UNABLE_TO_CONNECT = "Nie można połączyć się z PayPal."
			PAYPAL_TRY_LATER = "Spróbuj później."
			PAYPAL_ERROR_PROCESSING = "Nie można sprawdzić płatności."
			PAYPAL_CONTACT_LIBRARY = "Skontaktuj się z bibliotekarzem, aby sprawdzić płatność."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Powrót do szczegółów należności"
		}
	%]
*/