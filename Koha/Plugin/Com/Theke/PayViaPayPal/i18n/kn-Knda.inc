/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "(ದೋಷ) "
			error_title = "there was a problem processing your payment"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "{0} ಪೋಷಕರ ದಾಖಲೆಗಲು ಯಶಸ್ವಿಯಾಗಿ ಬಿಟ್ಟುಬಿಡಲಾಗಿದೆ or {1} ಪೋಷಕರ ದಾಖಲೆಗಲು ಯಶಸ್ವಿಯಾಗಿ ಅಳಿಸಿ ಹಾಕಲಾಗಿದೆ"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "&rsaquo; ಸೂಚಿಯ ಅಂಕಿಅಂಶಗಳು"
		}
	%]
*/