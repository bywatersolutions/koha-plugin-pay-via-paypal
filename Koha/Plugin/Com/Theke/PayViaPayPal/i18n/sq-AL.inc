/*
	[%
		TOKENS = {
			home_breadcrumb = "Faqja e parë"
			your_payment_breadcrumb = "Your Payment"
			error = "Gabim"
			error_title = "ka një problem me procesimin e pagesës tuaj"
			PAYPAL_UNABLE_TO_CONNECT = "Nuk mund të kryhet lidhja me PayPal."
			PAYPAL_TRY_LATER = "Provo përsëri më vonë."
			PAYPAL_ERROR_PROCESSING = "Nuk mund të bëhet verifikimi i pagesës."
			PAYPAL_CONTACT_LIBRARY = "Ju lutemi të kontaktoni me bibliotekën për të verifikuar pagesën."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Kthehu tek detajet e gjobës"
		}
	%]
*/