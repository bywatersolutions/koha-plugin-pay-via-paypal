/*
    [% 
        TOKENS = {
            home_breadcrumb="Inicio"
            your_payment_breadcrumb="Su Pago"
            error = "Error"
            error_title = "Hubo un error al procesar su pago"
            PAYPAL_UNABLE_TO_CONNECT = "No se pudo conectar a PayPal."
            PAYPAL_TRY_LATER = "Por favor, intente más tarde."
            PAYPAL_ERROR_PROCESSING = "No se pudo verificar su pago."
            PAYPAL_CONTACT_LIBRARY="Por favor, contacte a su biblioteca para confirmar su pago."
            PAYPAL_ERROR_STARTING = "No se pudo iniciar su pago."
            return = "Volver al detalle de cuenta"
        } 
    %]
*/