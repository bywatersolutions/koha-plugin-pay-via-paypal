/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "ስህተት፤"
			error_title = "there was a problem processing your payment"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "መረጃ"
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "ይህንን ውሰት ይመለስ "
		}
	%]
*/