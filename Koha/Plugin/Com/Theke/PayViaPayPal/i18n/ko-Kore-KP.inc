/*
	[%
		TOKENS = {
			home_breadcrumb = "홈으로"
			your_payment_breadcrumb = "Your Payment"
			error = "Error:"
			error_title = "입력한 사항에 문제가 있었습니다"
			PAYPAL_UNABLE_TO_CONNECT = "하나 이상의 태그를 추가할 수 없습니다."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "하나 이상의 태그를 추가할 수 없습니다."
			PAYPAL_CONTACT_LIBRARY = ". 더 많은 정보를 원하시면 도서관에 문의하시기 바랍니다."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "다음으로 되돌아가기:"
		}
	%]
*/