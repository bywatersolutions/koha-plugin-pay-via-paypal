/*
	[%
		TOKENS = {
			home_breadcrumb = "Ana sayfa"
			your_payment_breadcrumb = "Your Payment"
			error = "Hata"
			error_title = "Ödemenizin işlenmesinde bir sorun vardı"
			PAYPAL_UNABLE_TO_CONNECT = "PayPal ile bağlantı kurulamıyor."
			PAYPAL_TRY_LATER = "Lütfen daha sonra tekrar deneyin."
			PAYPAL_ERROR_PROCESSING = "Ödeme doğrulanamadı."
			PAYPAL_CONTACT_LIBRARY = "Lütfen ödemenizi doğrulamak için kütüphane ile irtibata geçin."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Ceza ayrıntılarına dön"
		}
	%]
*/