/*
	[%
		TOKENS = {
			home_breadcrumb = "Home"
			your_payment_breadcrumb = "Your Payment"
			error = "Error"
			error_title = "there was a problem processing your payment"
			PAYPAL_UNABLE_TO_CONNECT = "Unable to connect to PayPal."
			PAYPAL_TRY_LATER = "Please try again later."
			PAYPAL_ERROR_PROCESSING = "Unable to verify payment."
			PAYPAL_CONTACT_LIBRARY = "Please contact the library to verify your payment."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Return to fine details"
		}
	%]
*/