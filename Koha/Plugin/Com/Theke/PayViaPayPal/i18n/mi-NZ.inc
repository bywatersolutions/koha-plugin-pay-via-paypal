/*
	[%
		TOKENS = {
			home_breadcrumb = "Kāinga"
			your_payment_breadcrumb = "Your Payment"
			error = "Hapa"
			error_title = "kua rarua te tukatuka i tō utunga"
			PAYPAL_UNABLE_TO_CONNECT = "Tē taea te tūhono ki PayPal."
			PAYPAL_TRY_LATER = "Ngana anō ā muri ake."
			PAYPAL_ERROR_PROCESSING = "Tē taea tō utunga te manatoko."
			PAYPAL_CONTACT_LIBRARY = "Whakapā atu ki te whare pukapuka ki te manatoko i tō utunga."
			PAYPAL_ERROR_STARTING = "Unable to start your payment."
			return = "Hoki ki ngā taipitopito rauangi"
		}
	%]
*/