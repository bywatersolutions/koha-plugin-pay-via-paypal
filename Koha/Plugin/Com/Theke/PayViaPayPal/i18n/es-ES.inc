/*
	[%
		TOKENS = {
			home_breadcrumb = "Inicio"
			your_payment_breadcrumb = "Your Payment"
			error = "Error"
			error_title = "hubo un problema al procesar su pago"
			PAYPAL_UNABLE_TO_CONNECT = "No se puede conectar a PayPal."
			PAYPAL_TRY_LATER = "Por favor, intente de nuevo más tarde."
			PAYPAL_ERROR_PROCESSING = "No se pudo verificar el pago."
			PAYPAL_CONTACT_LIBRARY = "Por favor, contacte a la biblioteca para verificar su pago."
			PAYPAL_ERROR_STARTING = "No se pudo iniciar su pago."
			return = "Regresar a los detalles de multas"
		}
	%]
*/